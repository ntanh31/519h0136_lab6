import 'package:flutter/material.dart';
import '../controller/pizza_controller.dart';
import 'package:lab6_foodlist/data_provider.dart';
import '../models/pizza.dart';
import '../http/httprequest.dart';

class ListPizzaScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var pizzaController = DataProvider.of(context);
    return Scaffold(
      body: Container(
        child: FutureBuilder(
            future: pizzaController.callPizzas(),
            builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
              return ListView.builder(
                itemCount: pizzas.data?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  var itemData = pizzas.data![index];
                  return Dismissible(
                    key: Key(itemData.id.toString()),
                    child: ListTile(
                      title: Text(itemData.pizzaName),
                      subtitle: Text(
                          "${itemData.description } \$ ${itemData.price}"),
                    ),
                  );
                },
              );
            }
        ),
      ),
    );

  }


}
