import 'package:flutter/material.dart';
import 'dart:convert';
import '../models/pizza.dart';
import 'package:lab6_foodlist/controller/pizza_controller.dart';
import 'package:lab6_foodlist/data_provider.dart';

class HomeScreen extends StatelessWidget {

  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var pizzaController = DataProvider.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Container(
        child: FutureBuilder(
          future: pizzaController.callPizzas(),
          builder: (BuildContext context, AsyncSnapshot<List<Pizza>> pizzas) {
            return ListView.builder(
              itemCount: pizzas.data?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                var itemData = pizzas.data![index];
                return ListTile(
                  title: Text(itemData.pizzaName),
                  subtitle: Text(
                      "${itemData.description } \$ ${itemData.price}"),
                );
              },
            );
          },
        ),
      ),
    );
  }
}

