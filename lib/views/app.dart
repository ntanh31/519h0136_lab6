import 'package:flutter/material.dart';
import 'home_screen.dart';
import 'setting_screen.dart';
import 'platform_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Food App',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          visualDensity:VisualDensity.adaptivePlatformDensity
      ),
      home: MyHomePage(),
    );
  }

}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  int selectedIndexBottomMenu = 0;
  final screens = [
    HomeScreen(),
    SettingScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.notifications),
        onPressed: () async {
          // Get stored appCounter
          SharedPreferences prefs = await SharedPreferences.getInstance();

          var appCounter = prefs.getInt('appCounter') ?? 0;

          final alert = PlatformAlert(
            title: 'Times of opened app',
            message: 'Your have opened the app is $appCounter times.',
          );
          alert.show(context);
        },
      ),
      body: IndexedStack(
        children: screens,
        index: selectedIndexBottomMenu,
      ),
      bottomNavigationBar: _buildBottomMenu(),
    );
  }

  BottomNavigationBar _buildBottomMenu() {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          label: 'Settings',
        ),
      ],
      currentIndex: selectedIndexBottomMenu,
      onTap: _onBottomMenuTap,
    );
  }

  void _onBottomMenuTap(int value) {
    setState(() {
      selectedIndexBottomMenu = value;
    });
  }
}
